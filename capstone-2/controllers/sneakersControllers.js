// [Model Schema]
const Sneakers = require("../models/Sneakers.js")

// [Auth JWT]
const auth = require("../auth");

// Create Product (Admin Only)
module.exports.addSneaker = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){

		let newSneakers = new Sneakers({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			size: req.body.size,
			stocks: req.body.stocks,
			isActive: req.body.isActive
		})
		newSneakers.save()
		.then(save => res.send(true))
		.catch(error => res.send(false))

	} else{

			return res.send(false)
		}
}

// Retrieve all products (Admin Only)
module.exports.getAllSneakers = (req, res) =>{

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		Sneakers.find({})
		.then(result => res.send(result))
		.catch(error => res.send(false))
	}else{

		return res.send(false)
	}
}

// Retrieve all active products
module.exports.getAllActiveSneakers = (req, res) => {
	const active = {isActive: true}

	Sneakers.find(active)
	.then(result => res.send(result))
	.catch(error => res.send(false))
}

// Retrieve single products using Id
module.exports.getSingleSneaker = (req, res) => {

	const sneakerId = req.params.sneakerId

	Sneakers.findById(sneakerId)
	.then(result => res.send(result))
	.catch(error => res.send(false))
}

// Update Product Information(Admin only)
module.exports.updateSneaker = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const sneakerId = req.params.sneakerId

	if(userData.isAdmin){
		const updateSneaker = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			size: req.body.size,
		}
		Sneakers.findByIdAndUpdate(sneakerId, updateSneaker, {new:true})
		.then(result => res.send(true))
		.catch(error => res.send(false))

	}else{
			return res.send(false)
		}
}

// Archive Product (Admin Only)
module.exports.archiveSneaker = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const sneakerId = req.params.sneakerId;
	const archivedSneaker = {isActive: req.body.isActive}

	if(!userData.isAdmin){
		return res.send(false)
	}else{
		Sneakers.findByIdAndUpdate(sneakerId, archivedSneaker)
		.then(result => {
			if(req.body.isActive === false) {
				return res.send(true)

			}else{
				return res.send(true)
			}
		})
		.catch(error => res.send(false))

	}
}