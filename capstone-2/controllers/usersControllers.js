// [Models]
const Users = require ("../models/Users.js");
const bcrypt = require("bcrypt")

// [Auth JWT]
const auth = require("../auth.js");

// User Registration
module.exports.registerUser = (req, res) => {
	Users.findOne({email: req.body.email})
	.then(result => {
		if(result){
			return res.send(false)
		}else{
			// I add the condition for the userName so that user could also use this as their LogIn credential, username must be unique so even though the email is not use but if the userName is taken it will prompt an error. If the userName is still available and the email is not registered yet then registration will be successful.
			Users.findOne({userName: req.body.userName})
			.then(result =>{
				if(result){
					return res.send(false)
				}else{
					let newUser = new Users({
					firstName: req.body.firstName,
					lastName: req.body.lastName,
					userName: req.body.userName,
					email: req.body.email,
					password: bcrypt.hashSync(req.body.password, 10),
					isAdmin: req.body.isAdmin,
					mobileNo: req.body.mobileNo
				})
				newUser.save()
				.then(saved => res.send(true))
				.catch(error => res.send(false))
				}
			})
		}
	})
	.catch(error=> res.send(false));

}

// User Authentication
module.exports.loginUser = (req, res) =>{
	// passing the value of email and userName in the operator "$or" allows user to logIn using either userName or email because sometimes email is too long to use as logIn credential that's why I add username as a logIn credential for better user experience because they could just use their userName instead of their email.
	// I got this idea from stack overflow https://stackoverflow.com/questions/64017660/node-js-how-to-allow-user-to-login-with-username-or-email

	const login = {$or: [ {email: req.body.email}, {userName: req.body.userName} ]}

	Users.findOne(login)
	.then(result => {
		if(!result){
			return res.send(false)
		}else{

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({
					auth: auth.createAccessToken(result)
				})
			}else{
				return res.send(false);
			}
		}
	})
	.catch(error => res.send(false));
}

// Retrieve User Details using ID

module.exports.getUserDetails = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	Users.findById(userData.id)
	.then(result =>{
		result.password = "********";
		return res.send(result)
	})
	.catch(error => res.send(error))
}

// Set user as admin(Admin only)

module.exports.toAdmin = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const userId = req.params.userId;
	const toAdmin = {isAdmin: req.body.isAdmin};

	if(!userData.isAdmin){

		return res.send("You are not an Admin! You don't have access to this route!")
	}else{

		Users.findByIdAndUpdate(userId, toAdmin)
		.then(result =>{
			
			if(req.body.isAdmin === true){

				return res.send("User to Admin Successfully!")

			}else{

				return res.send("User to Non-Admin Successfully!")
			}
		})
		.catch(error => (error))
	}
}