const express = require('express');
const mongoose = require('mongoose')
const cors	= require("cors")
const port = 5001;
const app = express();
const usersRoutes = require("./routes/usersRoutes.js")
const sneakersRoutes = require("./routes/sneakersRoutes.js")
const ordersRoutes = require("./routes/ordersRoutes.js")



// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@batch288alegre.m7hdg38.mongodb.net/SneakersStoreAPI?retryWrites=true&w=majority", {useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

// to catch when there is an error in db
db.on('error', console.error.bind(console, `Network problem, can't connect to the db!`))

// to confirm that the connection is successful
db.once('open', () => console.log (`Connected to the database successfully!`))


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors())

// Routes
	
	// usersRoutes
	app.use("/users", usersRoutes);

	// sneakersRoutes
	app.use("/sneakers", sneakersRoutes);

	// ordersRoutes
	app.use("/orders", ordersRoutes);













app.listen(port, () => {
	console.log(`Server is running at port ${port}!`);
})