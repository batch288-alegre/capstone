import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link, useParams} from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function ActiveSneakersCard(props){
	const {user} = useContext(UserContext)

	const {_id, name, description, price, size, stocks, isActive} = props.sneakerProp;
	
	return(
						<Col className = "col-12 col-md-4 mx-auto mt-3">
							<Card className = "d-flex align-items-start">
							      <Card.Body>
							        <Card.Title>{name}</Card.Title>
							        <br/>
							        <Card.Subtitle>Description:</Card.Subtitle>
							        <Card.Text>
							        {description}
							        </Card.Text>

							        <Card.Subtitle>Price:</Card.Subtitle>
							        <Card.Text>
							        PHP {price}
							        </Card.Text>

							        <Card.Subtitle>Size</Card.Subtitle>
							        <Card.Text>
							        US {size}
							        </Card.Text>

							        <Card.Subtitle>Stocks</Card.Subtitle>
							        <Card.Text>
							        {stocks}
							        </Card.Text>

							        <Card.Subtitle>Available</Card.Subtitle>
							        <Card.Text>
							        {isActive ===true ? 'Yes' : 'No'}
							        </Card.Text>

							        {
							        	user.id !==null
							        	?
							        	<Button as = {Link} to = {`/activeSneakers/${_id}`} className = 'me-2 btn-buy'>BUY NOW</Button>
							        	:
							        	<Button as = {Link} to = '/login' className = 'me-2 btn-buy'>Log in to BUY!</Button>
							        }

							        
							      </Card.Body>
							    </Card>
						</Col>
		)
}