import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import{Link, NavLink} from 'react-router-dom';
import{useContext} from 'react';
import UserContext from '../UserContext.js';
import NavDropdown from 'react-bootstrap/NavDropdown';
import ReactDOM from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHouse, faStore } from '@fortawesome/free-solid-svg-icons'

export default function AppNavBar(){
	const home = <FontAwesomeIcon icon={faHouse} />
	const store = <FontAwesomeIcon icon={faStore} />

	const {user} = useContext(UserContext);

	return(
	<>
	  <Navbar bg="light" data-bs-theme="light">
	    <Container>
	      <Navbar.Brand as = {Link} to ='/'>Sneaker Store</Navbar.Brand>
	      <Nav className="ms-auto">
	        <Nav.Link as = {NavLink} to = '/'>{home}</Nav.Link>

{/*	        {
	        	user.id !== null && user.isAdmin
	        	?
	        	<></>
	        	:
	        	<Nav.Link as = {NavLink} to = '/activeSneakers' >Sneakers</Nav.Link>
	        }*/}
	        <Nav.Link as = {NavLink} to = '/activeSneakers' >{store}</Nav.Link>

	        {
	        	user.id === null
	        	?
	        	<>
		        	<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
		        	<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
	        	</>

	        	:
	        		<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
	        }
	        
	        {
	        	user.id !== null && user.isAdmin
	        	?

	        	<NavDropdown title="Admin Dashboard" id="basic-nav-dropdown">

	        	    <NavDropdown.Item as = {Link} to = '/addSneaker' >Add Sneaker</NavDropdown.Item>

	        	    <NavDropdown.Item as = {Link} to = '/allSneakers'>Retrieve All Sneakers</NavDropdown.Item>
	        	</NavDropdown>
	        	:
	        	<>
	        	</>
	        }

	      </Nav>
	    </Container>
	  </Navbar>
	</>
		)

}