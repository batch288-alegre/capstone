import {Button, Col, Container, Form, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom'
import Swal2 from 'sweetalert2'

import UserContext from '../UserContext.js'

export default function AddSneaker(){
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [size, setSize] = useState('');
	const [stocks, setStocks] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() =>{
		if(
			name !== '' &&
			description !== '' &&
			price !== '' &&
			size !== '' &&
			stocks !== ''
			){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	},[name, description, price, size, stocks])

		function addSneaker(e){

			e.preventDefault()

			fetch(`${process.env.REACT_APP_API_URL}/sneakers/addSneaker`,{
				method: 'POST',
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					size: size,
					stocks: stocks
				})
			})
			.then(response => response.json())
			.then(data => {
				if(data){
					setName('');
					setDescription('');
					setPrice('');
					setSize('');
					setStocks('');
					
					Swal2.fire({
						title: 'Successfully Added!',
						icon: 'success',
						text: 'Sneaker is successfully added!'

					})
					navigate('/allSneakers')
				}else{
					Swal2.fire({
					    title: 'Unable to Add!',
					    icon: 'error',
					    text: 'Unable to add sneaker. Please try again!'
					})
				}
			})
		}

	return(
		<Container className = "mt-5">
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Add Sneaker</h1>

					<Form onSubmit = {e => addSneaker(e)}>
					    <Form.Group className="mb-3" controlId="formBasicSneakerName">
					        <Form.Label>Sneaker Name</Form.Label>
					        <Form.Control 
					        	type="text"
					        	value = {name} 
					        	onChange = {e => {
					        	
					        		setName(e.target.value)
					        	}}
					        	placeholder="Sneaker Name" />

					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicDescription">
					        <Form.Label>Description</Form.Label>
					        <Form.Control 
					        	type="text"
					        	value = {description} 
					        	onChange = {e => {
					        	
					        		setDescription(e.target.value)
					        	}}
					        	placeholder="Description" />

					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicPrice">
					        <Form.Label>Price</Form.Label>
					        <Form.Control 
					        	type="number"
					        	value = {price} 
					        	onChange = {e => {
					        	
					        		setPrice(e.target.value)
					        	}}
					        	placeholder="Price" />

					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicSize">
					        <Form.Label>Sneaker Size</Form.Label>
					        <Form.Control 
					        	type="number"
					        	value = {size} 
					        	onChange = {e => {
					        	
					        		setSize(e.target.value)
					        	}}
					        	placeholder="Size" />

					    </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicStocks">
					        <Form.Label>Stocks</Form.Label>
					        <Form.Control 
					        	type="number"
					        	value = {stocks} 
					        	onChange = {e => {
					        	
					        		setStocks(e.target.value)
					        	}}
					        	placeholder="Stocks" />
					    </Form.Group>
					    <Button variant="primary" type="submit" disabled = {isDisabled}>Submit</Button>
					</Form>
				</Col>
			</Row>
		</Container>
		)



}