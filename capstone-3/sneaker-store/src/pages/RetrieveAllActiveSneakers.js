import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import AllActiveSneakersCard from '../components/AllActiveSneakersCard.js'
import {useParams} from 'react-router-dom';
import Swal2 from 'sweetalert2';


export default function RetrieveAllActiveSneakers(){
	const [sneakers, setSneakers] = useState([]);

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/sneakers/allActiveSneakers`)
		.then(result => result.json())
		.then(data => {
			setSneakers(data.map(sneaker => {
				return(
					<AllActiveSneakersCard key = {sneaker._id} sneakerProp = {sneaker}/>
					)
			}))
		})
	})

	return(
		<>
			<h1 className = 'text-center mt-3 mb-3'>SHOP NOW!</h1>
			<Container fluid='md' id = '#card-height'>
				<Row className = 'd-flex align-items-center justify-content-center ' >
				{sneakers}
				</Row>
			</Container>
		</>
		)
}