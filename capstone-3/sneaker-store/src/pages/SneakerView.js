import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Button, Card, Form} from 'react-bootstrap';
import{useParams, useNavigate} from 'react-router-dom'
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext.js';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSquarePlus, faSquareMinus } from '@fortawesome/free-solid-svg-icons'

export default function SneakerView(){
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [size, setSize] = useState('');
	const [quantity, setQuantity] = useState(1);
	const [totalAmount, setTotalAmount] = useState(price)

	const {user} = useContext(UserContext)

	const navigate = useNavigate();
	const {sneakerId} = useParams();
	// console.log(sneakerId)

	const increase = () => {
		setQuantity(quantity + 1);
		setTotalAmount((quantity + 1) * price)

	}

	const decrease = () => {
		setQuantity(quantity -1);
		setTotalAmount((quantity - 1) * price)
	}
	

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/sneakers/${sneakerId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
			setTotalAmount(data.price)
			setSize(data.size)

		})

	},[sneakerId])

	function checkOut(sneakerId){
		fetch(`${process.env.REACT_APP_API_URL}/orders/makeOrder`, {
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				sneakerId: sneakerId,
				quantity: quantity
			})

		})
		.then(response => response.json())
		.then(data =>{
			if(data){
				console.log(data.totalAmount)
				Swal2.fire({
				    title: 'Successfully Checkout!',
				    icon: 'success',
				    text: 'You have successfully check out this item.'
				})
				navigate('/activeSneakers')
			}else{
				Swal2.fire({
				    title: 'Something went wrong',
				    icon: 'error',
				    text: 'Please try again or log in first before checkout'
				})
			}
		})

	}

	const plus = <FontAwesomeIcon onClick= {increase} className = 'ms-2'icon={faSquarePlus} size = 'lg' />
	const minus = <FontAwesomeIcon onClick= {
		quantity ===0
		?
		null

		:
		() => decrease()}

		icon={faSquareMinus} size = 'lg' className = 'me-2'/>

	return(

		<Container className = 'mt-3' fluid='md'>
		    <Row>
		        <Col className	= 'col-md-8 mx-auto'>
		            <Card className = 'card-view'>
		                  <Card.Body>
		                    <Card.Title>HOT RIGHT NOW!</Card.Title>

		                    <Card.Subtitle className='my-3'>{name}</Card.Subtitle>

		                    <Card.Subtitle>Description</Card.Subtitle>
		                    <Card.Text>{description}</Card.Text>

		                    <Card.Subtitle>Price</Card.Subtitle>
		                    <Card.Text>₱ {price.toLocaleString()}.00</Card.Text>

		                    <Card.Subtitle>Size</Card.Subtitle>
		                    <Card.Text>US {size}</Card.Text>

		                     <Card.Subtitle className = 'mb-2'>Quantity</Card.Subtitle>
		                     <Card.Text>
		                     {minus}
		                     {quantity}
		                     {plus}
		                     </Card.Text>

		                    <Card.Subtitle className= 'mt-2'>Amount</Card.Subtitle>
		                    <Card.Text className="total">₱ {totalAmount.toLocaleString()}.00</Card.Text>

{/*		                    <Form className="quantityForm">
									<Form.Group className="mb-3">
									<Form.Control 
									type="number" 
									id="#quantity-amount"
									value={quantity}
									onChange={productId => setQuantity(productId.target.value)}
									required
									/>
									</Form.Group>
								</Form>*/}


		                    {
		                    	user.isAdmin
		                    	?
		                    	<Button variant="primary" disabled = {true}>ADMIN CANNOT CHECKOUT</Button>
		                    	:
		                    	<Button variant="primary" onClick ={()=> checkOut(sneakerId)}>Checkout</Button>
		                    }
		                  </Card.Body>
		             </Card>
		        </Col>
		    </Row>
		</Container>

		)	
}