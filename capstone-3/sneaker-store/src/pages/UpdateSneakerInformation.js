import {Container, Row, Col, Card, Button, Form, Modal} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';

export default function UpdateSneakerInformation(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const {sneakerId} = useParams();
	// console.log(sneakerId)

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [size, setSize] = useState(0);
	const [stocks, setStocks] = useState(0);

	useEffect (() => {
		fetch(`${process.env.REACT_APP_API_URL}/sneakers/${sneakerId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description)
			setPrice(data.price)
			setSize(data.size)
			setStocks(data.stocks)
		})
	},[])

	function UpdateSneaker(e){
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/sneakers/${sneakerId}/UpdateSneaker`,{
			method: 'PATCH',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				size: size,
				stocks: stocks
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Successfully updated!',
					icon: 'success',
					text: 'Current sneaker information is updated.'
				})
				navigate('/allSneakers')
			}else{
				Swal2.fire({
				    title: 'Unable to updated!',
				    icon: 'error',
				    text: 'Unable to updated sneaker information please try again.'
				})
			}
		})
	}





	return(
		<>
			<Container className = "mt-5">
				<Row>
					<Col className = "col-6 mx-auto">
						<h1 className = "text-center">Update Sneaker</h1>

						<Form onSubmit = {e => UpdateSneaker(e)}>
						    <Form.Group className="mb-3" controlId="formBasicSneakerName">
						        <Form.Label>Sneaker Name</Form.Label>
						        <Form.Control 
						        	type="text"
						        	value = {name} 
						        	onChange = {e => {
						        	
						        		setName(e.target.value)
						        	}}
						        	placeholder="Sneaker Name" />

						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicDescription">
						        <Form.Label>Description</Form.Label>
						        <Form.Control 
						        	type="text"
						        	value = {description} 
						        	onChange = {e => {
						        	
						        		setDescription(e.target.value)
						        	}}
						        	placeholder="Description" />

						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicPrice">
						        <Form.Label>Price</Form.Label>
						        <Form.Control 
						        	type="number"
						        	value = {price} 
						        	onChange = {e => {
						        	
						        		setPrice(e.target.value)
						        	}}
						        	placeholder="Price" />

						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicSize">
						        <Form.Label>Sneaker Size</Form.Label>
						        <Form.Control 
						        	type="number"
						        	value = {size} 
						        	onChange = {e => {
						        	
						        		setSize(e.target.value)
						        	}}
						        	placeholder="Size" />

						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicStocks">
						        <Form.Label>Stocks</Form.Label>
						        <Form.Control 
						        	type="number"
						        	value = {stocks} 
						        	onChange = {e => {
						        	
						        		setStocks(e.target.value)
						        	}}
						        	placeholder="Stocks" />
						    </Form.Group>
						    <Button variant="primary" type="submit">Submit</Button>
						</Form>
					</Col>
				</Row>
			</Container>
			)
		</>
		)

}