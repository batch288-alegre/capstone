import {Navigate, useParams, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext.js';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext.js';



export default function Archive(){
	const {setUser} = useContext(UserContext);
	const {sneakerId} = useParams();

	fetch(`${process.env.REACT_APP_API_URL}/sneakers/${sneakerId}`,{
		'method': 'PATCH',
		'headers': {
			'Authorization': `Bearer ${localStorage.getItem('token')}`,
			'Content-Type': 'application/json'
		}
	})
	.then(result => result.json())
	.then(data => {
		<AllSneakersCard key = {sneaker._id} sneakerProp = {sneaker}/>
	})
	return(

		<Navigate to = "/allSneakers" />

		)
}